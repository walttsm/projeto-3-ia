#!/usr/bin/env python3

"""Exemplo de utilizacao de imagens em python3 com Pillow"""

from PIL import Image

__author__ = "Walter Schmidt Marinho"
__copyright__ = "Copyleft"
__credits__ = ["python3 wikibooks"]
__license__ = "GPLv3"
__version__ = "1.0"
__maintainer__ = "Walter Marinho"
__email__ = "waltersmarinho@edu.unifil.br"
__status__ = "Entregue"

if __name__ == "__main__":
    # cria uma nova imagem RGB, toda preta
    img = Image.new( 'RGB', (255,255), "black")

    # cria a matriz de pixels
    pixels = img.load()

    # para cada pixel:
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            # define uma cor (R, G, B)
            if ((i > img.size[0] / 4 and i < 3 * img.size[0] / 4) and (j > img.size[1] / 4 and j < 3 * img.size[1] / 4 )):
                pixels[i,j] = (0, 0, 255)
            elif ((i > img.size[0] / 4 - 10 and i < 3 * img.size[0] / 4 + 10) and (j > img.size[1] / 4 - 10 and j < 3 * img.size[1] / 4 + 10)):
                pixels[i,j] = (255, 0, 0)
            else:
                pixels[i,j] = (0, 255, 0)
            
            #pixels[i,j] = (i, j, 100)

    img.show()
    img.save("exemplo.png")
